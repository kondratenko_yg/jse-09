package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.service.TaskService;
import ru.kondratenko.tm.entity.Task;

public class TaskController extends AbstractController {

    public final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
        final String description = scanner.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK INDEX:]");
        final Task task = taskService.findByIndex(inputIndexCheckFormat());
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            taskService.update(task.getId(), name, description);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK ID:]");
        final Task task = taskService.findById(inputIdCheckFormat());
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            taskService.update(task.getId(), name, description);
            System.out.println("[OK]");
            return 0;
        }
    }

    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName() {
        System.out.println("[CLEAR TASK BY NAME]");
        System.out.println("ENTER TASK NAME: ");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[CLEAR TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX: ");
        final Task task = taskService.removeByIndex(inputIndexCheckFormat());
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() {
        System.out.println("[CLEAR TASK BY ID]");
        System.out.println("ENTER TASK ID: ");
        final Task task = taskService.removeById(inputIdCheckFormat());
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("ENTER TASK INDEX: ");
        final Task task = taskService.findByIndex(inputIndexCheckFormat());
        viewTask(task);
        return 0;
    }

    public int viewTaskById() {
        System.out.println("ENTER TASK ID: ");
        final Task task = taskService.findById(inputIdCheckFormat());
        viewTask(task);
        return 0;
    }

    public int listTask() {
        System.out.println("[LIST TASK]");
        int index = 1;
        for (final Task task : taskService.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

}
